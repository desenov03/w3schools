import pickle
import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, OneHotEncoder
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline

df = pd.read_csv('data.csv')

categorical_features = ['Weather Description']
numeric_features = ['Temperature']

X = df.drop(['Energy', 'Danceability', 'Tempo (BPM)', 'Speechiness'], axis=1)
y = df[['Energy', 'Danceability', 'Tempo (BPM)', 'Speechiness']]
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=42)

categorical_transformer = OneHotEncoder(handle_unknown='ignore', sparse=False, categories='auto', drop='first')
numeric_transformer = StandardScaler()

preprocessor = ColumnTransformer(
    transformers=[
        ('cat', categorical_transformer, categorical_features),
        ('num', numeric_transformer, numeric_features)
    ],
    remainder='passthrough'
)

pipeline = Pipeline([
    ('preprocessor', preprocessor),
    ('model', LinearRegression())
])

pipeline.fit(X_train, y_train)

with open('model.pkl', 'wb') as f:
    pickle.dump(pipeline, f)

import seaborn as sns
import matplotlib.pyplot as plt
X_transformed = preprocessor.transform(X)
columns_after_encoding = (list(preprocessor.transformers_[0][1].get_feature_names_out(categorical_features)) +
                          numeric_features)
X_transformed_df = pd.DataFrame(X_transformed, columns=columns_after_encoding)
correlation_data_transformed = pd.concat([X_transformed_df, y], axis=1)
correlation_matrix_transformed = correlation_data_transformed.corr()
plt.figure(figsize=(12, 10))
sns.heatmap(correlation_matrix_transformed, annot=True, cmap='coolwarm', fmt=".2f", linewidths=0.5)
plt.title('Correlation Matrix after Preprocessing')
plt.show()