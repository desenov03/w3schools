import pandas as pd
import numpy as np

health_data = pd.read_csv("main.csv", header=0, sep=",")

var = np.var(health_data)

print(var)
print(max(var))
print(min(var))
