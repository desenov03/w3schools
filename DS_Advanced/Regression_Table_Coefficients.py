def Predict_Calorie_Burnage(Duration):
    return (5.6642 * Duration + 18.3665)


# Try some different values:
print(Predict_Calorie_Burnage(45))
print(Predict_Calorie_Burnage(60))
print(Predict_Calorie_Burnage(80))
print(Predict_Calorie_Burnage(120))
