import numpy as np

danceability = [0.888, 0.65, 0.79, 0.33, 0.45]
max_danceability = max(danceability)
min_danceability = min(danceability)
mean_danceability = np.mean(danceability)

print("Максимальное значение danceability:", max_danceability)
print("Минимальное значение danceability:", min_danceability)
print("Среднее значение danceability:", mean_danceability)