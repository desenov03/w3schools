import pandas as pd
import matplotlib.pyplot as plt
from scipy import stats
from scipy.optimize import curve_fit

full_health_data = pd.read_csv("main.csv", header=0, sep=",")

x = full_health_data["Average_Pulse"]
y = full_health_data["Calorie_Burnage"]


def polynomial_func(x, a, b, c):
 return a * x ** 2 + b * x + c


params, covariance = curve_fit(polynomial_func, x, y)

a, b, c = params

predicted_values = polynomial_func(x, a, b, c)

plt.scatter(x, y)
plt.plot(x, predicted_values)
plt.ylim(ymin=0, ymax=2000)
plt.xlim(xmin=0, xmax=200)
plt.xlabel("Average_Pulse")
plt.ylabel("Calorie_Burnage")
plt.show()



