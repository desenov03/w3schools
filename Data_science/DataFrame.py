import pandas as pd

d = {'track': ['GimmeGimmeMore','Staiyn Alive','Вредина','Summertimes Sadness','!Dance'], 'danceability': [0.888, 0.65, 0.79, 0.33, 0.45], 'energy': [0.755, 0.23, 0.9, 0.54, 0.36], 'acousticness': [0.22, 0.1, 0.23, 0.44, 0.36]}

df = pd.DataFrame(data=d)

print(df)
