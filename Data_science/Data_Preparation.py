import pandas as pd

df = pd.DataFrame({
    "Name": ["John Doe", "Jane Doe", "John Smith", "Jane Smith"],
    "Age": [30, 28, 29, 27],
    "Gender": ["M", "F", "M", "F"]
})
print(df.head())
print(df.shape)
print(df.dtypes)