import pandas as pd
import matplotlib.pyplot as plt

health_data = pd.read_csv("main.csv", header=0, sep=",")

health_data.plot(x ='Calorie_Burnage', y='Duration', kind='line')
plt.ylim(ymin=0)
plt.xlim(xmin=0)

plt.show()
