import numpy as np
import matplotlib.pyplot as plt
x = np.array([40, 30, 20, 10])
y = np.array([60, 60, 50, 50])

m = (y[1] - y[0]) / (x[1] - x[0])
b = y[0] - m * x[0]

print("Наклон:", m)
print("Пересечение:", b)
import pandas as pd
import numpy as np

health_data = pd.read_csv("main.csv", header=0, sep=",")

x = health_data["Calorie_Burnage"]
y = health_data["Duration"]
slope_intercept = np.polyfit(x,y,1)

print(slope_intercept)
#[0.14545455 9.54545455]


health_data = pd.read_csv("main.csv", header=0, sep=",")

health_data.plot(x ='Calorie_Burnage', y='Duration', kind='line'),
plt.ylim(ymin=30, ymax=70)
plt.xlim(xmin=250, xmax=400)

plt.show()
